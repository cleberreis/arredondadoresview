//
//  ViewArredondada.swift
//  ArredondadoresView
//
//  Created by Cleber Reis on 12/11/18.
//  Copyright © 2018 Cleber Reis. All rights reserved.
//

import UIKit

public class ViewArredondada: UIView {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override public func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
        
        
    }
 

}
